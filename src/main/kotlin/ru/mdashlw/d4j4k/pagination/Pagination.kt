package ru.mdashlw.d4j4k.pagination

import discord4j.core.`object`.entity.Message
import discord4j.core.`object`.entity.MessageChannel
import discord4j.core.`object`.reaction.ReactionEmoji
import discord4j.core.`object`.util.Snowflake
import discord4j.core.event.EventDispatcher
import discord4j.core.event.domain.message.ReactionAddEvent
import discord4j.core.spec.EmbedCreateSpec
import reactor.core.publisher.Mono
import ru.mdashlw.d4j4k.extensions.doOnError
import ru.mdashlw.d4j4k.extensions.on
import java.time.Duration
import java.util.concurrent.TimeoutException

class Pagination<T>(
    private val eventDispatcher: EventDispatcher,
    private val channel: MessageChannel,
    private val users: List<Snowflake>,
    itemsOnPage: Int,
    private val duration: Duration,
    content: Collection<T>,
    private val creator: Collection<T>.(embed: EmbedCreateSpec) -> Unit
) {
    private val chunked = content.chunked(itemsOnPage)

    private var page: Int = 0

    lateinit var message: Message

    private inline val total: Int
        get() = chunked.size

    private val embed: (EmbedCreateSpec) -> Unit
        get() = {
            it.setFooter("Page ${page + 1} / $total", null)
            creator(getContent(page), it)
        }

    fun init(): Mono<Void> =
        channel.createEmbed(embed)
            .filter { total != 1 }
            .doOnNext {
                message = it

                it.addReaction(arrowLeft).subscribe()
                it.addReaction(arrowRight).subscribe()
            }
            .doOnSubscribe {
                eventDispatcher
                    .on<ReactionAddEvent>(0, duration)
                    .filter { it.channelId == channel.id }
                    .filter { it.messageId == message.id }
                    .filter { it.userId in users }
                    .doOnError(TimeoutException::class) {
                        message.removeSelfReaction(arrowLeft).subscribe()
                        message.removeSelfReaction(arrowRight).subscribe()
                    }
                    .subscribe { event ->
                        val emoji = event.emoji.asUnicodeEmoji()

                        emoji
                            .filter { it == arrowLeft }
                            .filter { canPaginate(page - 1) }
                            .ifPresent {
                                paginate(page - 1)

                                message.removeReaction(it, event.userId).subscribe()
                            }

                        emoji
                            .filter { it == arrowRight }
                            .filter { canPaginate(page + 1) }
                            .ifPresent {
                                paginate(page + 1)

                                message.removeReaction(it, event.userId).subscribe()
                            }
                    }
            }
            .then()

    private fun getContent(page: Int): Collection<T> = chunked[page]

    private fun canPaginate(page: Int): Boolean = page in 0 until total

    private fun paginate(page: Int) {
        this.page = page

        message.edit {
            it.setEmbed(embed)
        }.subscribe()
    }

    companion object {
        val arrowLeft: ReactionEmoji = ReactionEmoji.unicode("\u2B05")
        val arrowRight: ReactionEmoji = ReactionEmoji.unicode("\u27A1")
    }
}
