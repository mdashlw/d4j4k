package ru.mdashlw.d4j4k.extensions

import discord4j.core.`object`.entity.Message
import discord4j.core.`object`.entity.TextChannel
import discord4j.core.`object`.util.Snowflake
import reactor.core.publisher.Flux
import ru.mdashlw.util.matchresult.get

@JvmField
val CHANNEL_MENTION_PATTERN = "<#(\\d+)>".toRegex()

@Suppress("NestedLambdaShadowedImplicitParameter")
val Message.channelMentionIds: Set<Snowflake>
    get() = content
        .map {
            CHANNEL_MENTION_PATTERN.findAll(it)
                .map { it[1].asSnowflake() }
                .toSet()
        }
        .orElse(emptySet())

val Message.channelMentions: Flux<TextChannel>
    get() = channelMentionIds.toFlux()
        .flatMap { client.getChannelById(it) }
        .cast()
