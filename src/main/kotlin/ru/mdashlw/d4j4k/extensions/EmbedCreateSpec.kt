@file:Suppress("NOTHING_TO_INLINE")

package ru.mdashlw.d4j4k.extensions

import discord4j.core.spec.EmbedCreateSpec

inline fun EmbedCreateSpec.addBlankField(inline: Boolean): EmbedCreateSpec =
    addField("\u200E", "\u200E", inline)
