package ru.mdashlw.d4j4k.extensions

import discord4j.core.`object`.entity.TextChannel
import discord4j.core.`object`.util.Permission
import reactor.core.publisher.Mono

val TextChannel.canTalk: Mono<Boolean>
    get() =
        client.selfId
            .map {
                getEffectivePermissions(it)
                    .map { (Permission.VIEW_CHANNEL.value or Permission.SEND_MESSAGES.value) and it.rawValue > 0 }
            }
            .orElse(false.toMono())
