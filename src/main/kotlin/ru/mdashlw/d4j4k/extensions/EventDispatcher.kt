@file:Suppress("NOTHING_TO_INLINE")

package ru.mdashlw.d4j4k.extensions

import discord4j.core.event.EventDispatcher
import discord4j.core.event.domain.Event
import reactor.core.publisher.Flux
import java.time.Duration
import kotlin.reflect.KClass

inline fun <reified T : Event> EventDispatcher.on(eventClass: KClass<T> = T::class): Flux<T> = on(eventClass.java)

inline fun <reified T : Event> EventDispatcher.on(eventClass: KClass<T>, amount: Long, duration: Duration): Flux<T> =
    on(eventClass.java)
        .run { amount.takeIf { it != 0L }?.let(::take) ?: this }
        .timeout(duration)

inline fun <reified T : Event> EventDispatcher.on(amount: Long, duration: Duration): Flux<T> =
    on(T::class, amount, duration)
