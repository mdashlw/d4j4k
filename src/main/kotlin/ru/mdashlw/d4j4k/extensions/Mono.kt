@file:Suppress("NOTHING_TO_INLINE")

package ru.mdashlw.d4j4k.extensions

import org.reactivestreams.Publisher
import reactor.core.publisher.Mono
import java.util.concurrent.Callable
import java.util.concurrent.CompletableFuture
import kotlin.reflect.KClass

inline fun <T> Publisher<T>.toMono(): Mono<T> = Mono.from(this)

inline fun <T> (() -> T?).toMono(): Mono<T> = Mono.fromSupplier(this)

inline fun <T : Any> T.toMono(): Mono<T> = Mono.just(this)

inline fun <T> CompletableFuture<out T?>.toMono(): Mono<T> = Mono.fromFuture(this)

inline fun <T> Callable<T?>.toMono(): Mono<T> = Mono.fromCallable(::call)

inline fun <T> Throwable.toMono(): Mono<T> = Mono.error(this)

inline fun <reified T : Any> Mono<*>.cast(): Mono<T> = cast(T::class.java)

inline fun <T, E : Throwable> Mono<T>.doOnError(type: KClass<E>, noinline onError: (E) -> Unit): Mono<T> =
    doOnError(type.java, onError)

inline fun <T, E : Throwable> Mono<T>.onErrorMap(type: KClass<E>, noinline mapper: (E) -> Throwable): Mono<T> =
    onErrorMap(type.java, mapper)

inline fun <reified T : Any> Mono<*>.ofType(): Mono<T> = ofType(T::class.java)

inline fun <T : Any, E : Throwable> Mono<T>.onErrorResume(type: KClass<E>, noinline fallback: (E) -> Mono<T>): Mono<T> =
    onErrorResume(type.java, fallback)

inline fun <T : Any, E : Throwable> Mono<T>.onErrorReturn(type: KClass<E>, fallbackValue: T): Mono<T> =
    onErrorReturn(type.java, fallbackValue)

inline fun <T> Mono<T>.switchIfEmpty(noinline alternate: () -> Mono<T>): Mono<T> = switchIfEmpty(Mono.defer(alternate))
