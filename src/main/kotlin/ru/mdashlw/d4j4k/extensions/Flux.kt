@file:Suppress("NOTHING_TO_INLINE")

package ru.mdashlw.d4j4k.extensions

import org.reactivestreams.Publisher
import reactor.core.publisher.Flux
import ru.mdashlw.d4j4k.util.toIterable
import java.util.stream.Stream
import kotlin.reflect.KClass

inline fun <T : Any> Publisher<T>.toFlux(): Flux<T> = Flux.from(this)

inline fun <T : Any> Iterator<T>.toFlux(): Flux<T> = toIterable().toFlux()

inline fun <T : Any> Iterable<T>.toFlux(): Flux<T> = Flux.fromIterable(this)

inline fun <T : Any> Sequence<T>.toFlux(): Flux<T> = toIterable().toFlux()

inline fun <T : Any> Stream<T>.toFlux(): Flux<T> = Flux.fromStream(this)

inline fun BooleanArray.toFlux(): Flux<Boolean> = toList().toFlux()

inline fun ByteArray.toFlux(): Flux<Byte> = toList().toFlux()

inline fun ShortArray.toFlux(): Flux<Short> = toList().toFlux()

inline fun IntArray.toFlux(): Flux<Int> = toList().toFlux()

inline fun LongArray.toFlux(): Flux<Long> = toList().toFlux()

inline fun FloatArray.toFlux(): Flux<Float> = toList().toFlux()

inline fun DoubleArray.toFlux(): Flux<Double> = toList().toFlux()

inline fun <T> Array<out T>.toFlux(): Flux<T> = Flux.fromArray(this)

inline fun <T> Throwable.toFlux(): Flux<T> = Flux.error(this)

inline fun <reified T : Any> Flux<*>.cast(): Flux<T> = cast(T::class.java)

inline fun <T, E : Throwable> Flux<T>.doOnError(type: KClass<E>, noinline onError: (E) -> Unit): Flux<T> =
    doOnError(type.java, onError)

inline fun <T, E : Throwable> Flux<T>.onErrorMap(type: KClass<E>, noinline mapper: (E) -> Throwable): Flux<T> =
    onErrorMap(type.java, mapper)

inline fun <reified T : Any> Flux<*>.ofType(): Flux<T> = ofType(T::class.java)

inline fun <T : Any, E : Throwable> Flux<T>.onErrorResume(
    type: KClass<E>,
    noinline fallback: (E) -> Publisher<T>
): Flux<T> =
    onErrorResume(type.java, fallback)

inline fun <T : Any, E : Throwable> Flux<T>.onErrorReturn(type: KClass<E>, fallbackValue: T): Flux<T> =
    onErrorReturn(type.java, fallbackValue)

inline fun <T : Any> Flux<out Iterable<T>>.split(): Flux<T> = flatMapIterable { it }

inline fun <T> Flux<T>.switchIfEmpty(noinline alternate: () -> Publisher<T>): Flux<T> =
    switchIfEmpty(Flux.defer(alternate))
