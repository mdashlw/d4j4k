@file:Suppress("NOTHING_TO_INLINE")

package ru.mdashlw.d4j4k.extensions

import discord4j.core.`object`.util.Snowflake

inline fun Long.asSnowflake(): Snowflake = Snowflake.of(this)
