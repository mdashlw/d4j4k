@file:Suppress("NOTHING_TO_INLINE")

package ru.mdashlw.d4j4k.util

// TODO Move to ru.mdashlw.util:common-util
internal inline fun <reified T> Sequence<Any>.mapAs(): Sequence<T> = map { it as T }

// TODO Move to ru.mdashlw.util:common-util
@PublishedApi
internal inline fun <T> Sequence<T>.toIterable() =
    object : Iterable<T> {
        override fun iterator(): Iterator<T> = this@toIterable.iterator()
    }
