@file:Suppress("NOTHING_TO_INLINE")

package ru.mdashlw.d4j4k.util

// TODO Move to ru.mdashlw.util:common-util
internal inline fun CharSequence.count(char: Char) = count { it == char }
