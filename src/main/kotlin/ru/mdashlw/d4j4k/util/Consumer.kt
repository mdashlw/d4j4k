package ru.mdashlw.d4j4k.util

// TODO Move to ru.mdashlw.util:common-util
internal inline fun <T, R> ((T) -> R).combine(crossinline other: (T) -> R): (T) -> R = {
    this(it)
    other(it)
}
