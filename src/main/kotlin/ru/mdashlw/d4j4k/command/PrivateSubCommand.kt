package ru.mdashlw.d4j4k.command

import discord4j.core.`object`.util.Snowflake
import reactor.core.publisher.Mono
import ru.mdashlw.d4j4k.extensions.toMono

abstract class PrivateSubCommand(parent: Command) : SubCommand(parent) {
    open val users: Set<Snowflake> = (parent as? PrivateCommand)?.users ?: emptySet()

    override fun checkAccess(event: Event): Mono<Boolean> =
        (event.member.id.run { equals(DiscordCommandClient.INSTANCE.owner) || this in users }).toMono()
}
