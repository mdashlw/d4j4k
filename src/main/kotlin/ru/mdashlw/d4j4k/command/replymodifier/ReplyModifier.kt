package ru.mdashlw.d4j4k.command.replymodifier

import discord4j.core.spec.EmbedCreateSpec
import reactor.core.publisher.Mono
import ru.mdashlw.d4j4k.command.Command
import ru.mdashlw.d4j4k.command.DiscordCommandClient
import ru.mdashlw.d4j4k.extensions.toMono

interface ReplyModifier {
    fun check(command: Command, event: Command.Event): Mono<Boolean> = true.toMono()

    fun modify(command: Command, event: Command.Event): Mono<(EmbedCreateSpec) -> Unit>

    fun register() {
        DiscordCommandClient.INSTANCE.replyModifiers += this
    }
}
