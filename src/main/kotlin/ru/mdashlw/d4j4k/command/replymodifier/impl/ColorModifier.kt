package ru.mdashlw.d4j4k.command.replymodifier.impl

import discord4j.core.spec.EmbedCreateSpec
import reactor.core.publisher.Mono
import ru.mdashlw.d4j4k.command.Command
import ru.mdashlw.d4j4k.command.DiscordCommandClient
import ru.mdashlw.d4j4k.command.replymodifier.ReplyModifier
import ru.mdashlw.d4j4k.extensions.toMono

object ColorModifier : ReplyModifier {
    override fun modify(command: Command, event: Command.Event): Mono<(EmbedCreateSpec) -> Unit> =
        { embed: EmbedCreateSpec ->
            DiscordCommandClient.INSTANCE.colors.default?.let { color ->
                embed.setColor(color)
            }

            Unit
        }.toMono()
}
