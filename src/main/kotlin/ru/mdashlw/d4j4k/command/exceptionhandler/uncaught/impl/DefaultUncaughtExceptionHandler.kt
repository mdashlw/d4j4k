package ru.mdashlw.d4j4k.command.exceptionhandler.uncaught.impl

import ru.mdashlw.d4j4k.command.Command
import ru.mdashlw.d4j4k.command.DiscordCommandClient
import ru.mdashlw.d4j4k.command.exceptionhandler.uncaught.UncaughtExceptionHandler

object DefaultUncaughtExceptionHandler : UncaughtExceptionHandler() {
    override fun handle(command: Command, event: Command.Event, exception: Throwable) {
        exception.printStackTrace()

        event.client.getUserById(DiscordCommandClient.INSTANCE.owner)
            .flatMap {
                event.reply { embed ->
                    embed.setColor(DiscordCommandClient.INSTANCE.colors.error)
                    embed.setTitle("Uncaught Error")
                    embed.setDescription(exception.toString())
                    embed.setFooter("Report to the developer, ${it.username}#${it.discriminator}", it.avatarUrl)
                }
            }
            .subscribe()
    }
}
