package ru.mdashlw.d4j4k.command.exceptionhandler.impl

import discord4j.core.`object`.util.Permission
import ru.mdashlw.d4j4k.command.Command
import ru.mdashlw.d4j4k.command.exceptionhandler.ExceptionHandler
import ru.mdashlw.d4j4k.command.exceptions.NoMemberPermissionsException

object NoMemberPermissionsHandler :
    ExceptionHandler<NoMemberPermissionsException>(NoMemberPermissionsException::class) {
    override fun handle(command: Command, event: Command.Event, exception: NoMemberPermissionsException) {
        event.replyError(
            "You need the following permissions to execute this command:\n" +
                    command.meta.memberPermissions.joinToString("\n", "**", "**", transform = Permission::name)
        ).subscribe()
    }
}
