package ru.mdashlw.d4j4k.command.exceptionhandler.impl

import ru.mdashlw.d4j4k.command.Command
import ru.mdashlw.d4j4k.command.DiscordCommandClient
import ru.mdashlw.d4j4k.command.exceptionhandler.ExceptionHandler
import ru.mdashlw.d4j4k.command.exceptions.IllegalUsageException

object IllegalUsageHandler : ExceptionHandler<IllegalUsageException>(IllegalUsageException::class) {
    override fun handle(command: Command, event: Command.Event, exception: IllegalUsageException) {
        event.reply { embed ->
            embed.setColor(DiscordCommandClient.INSTANCE.colors.error)
            embed.setTitle("Error")
            embed.setDescription(exception.message.toString())
            embed.setFooter("${event.guildSettings.prefix}${command.meta.usage}", null)
        }.subscribe()
    }
}
