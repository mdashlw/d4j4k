package ru.mdashlw.d4j4k.command.exceptionhandler.impl

import discord4j.core.`object`.util.Permission
import ru.mdashlw.d4j4k.command.Command
import ru.mdashlw.d4j4k.command.exceptionhandler.ExceptionHandler
import ru.mdashlw.d4j4k.command.exceptions.NoSelfPermissionsException

object NoSelfPermissionsHandler : ExceptionHandler<NoSelfPermissionsException>(NoSelfPermissionsException::class) {
    override fun handle(command: Command, event: Command.Event, exception: NoSelfPermissionsException) {
        event.replyError(
            "I need the following permissions to perform this command:\n" +
                    command.meta.selfPermissions.joinToString("\n", "**", "**", transform = Permission::name)
        ).subscribe()
    }
}
