package ru.mdashlw.d4j4k.command.exceptionhandler.impl

import ru.mdashlw.d4j4k.command.Command
import ru.mdashlw.d4j4k.command.exceptionhandler.ExceptionHandler

object CommandHelpHandler : ExceptionHandler<Command.Help>(Command.Help::class) {
    override fun handle(command: Command, event: Command.Event, exception: Command.Help) {
        event.replyHelp().subscribe()
    }
}
