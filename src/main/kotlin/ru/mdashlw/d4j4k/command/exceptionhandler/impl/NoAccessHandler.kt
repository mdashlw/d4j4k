package ru.mdashlw.d4j4k.command.exceptionhandler.impl

import ru.mdashlw.d4j4k.command.Command
import ru.mdashlw.d4j4k.command.exceptionhandler.ExceptionHandler
import ru.mdashlw.d4j4k.command.exceptions.NoAccessException

object NoAccessHandler : ExceptionHandler<NoAccessException>(NoAccessException::class) {
    override fun handle(command: Command, event: Command.Event, exception: NoAccessException) {
        event.replyError("You do not have access to use this command").subscribe()
    }
}
