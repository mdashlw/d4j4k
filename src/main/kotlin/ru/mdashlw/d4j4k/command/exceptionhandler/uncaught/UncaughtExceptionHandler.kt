package ru.mdashlw.d4j4k.command.exceptionhandler.uncaught

import ru.mdashlw.d4j4k.command.Command
import ru.mdashlw.d4j4k.command.exceptionhandler.ExceptionHandler

abstract class UncaughtExceptionHandler : ExceptionHandler<Throwable>(Throwable::class) {
    abstract override fun handle(command: Command, event: Command.Event, exception: Throwable)
}
