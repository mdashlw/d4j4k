package ru.mdashlw.d4j4k.command.exceptionhandler.impl

import ru.mdashlw.d4j4k.command.Command
import ru.mdashlw.d4j4k.command.exceptionhandler.ExceptionHandler

object CommandSuccessHandler : ExceptionHandler<Command.Success>(Command.Success::class) {
    override fun handle(command: Command, event: Command.Event, exception: Command.Success) {
        event.replySuccess(exception.message ?: "(no message)").subscribe()
    }
}
