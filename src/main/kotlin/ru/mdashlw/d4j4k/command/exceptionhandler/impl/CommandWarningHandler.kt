package ru.mdashlw.d4j4k.command.exceptionhandler.impl

import ru.mdashlw.d4j4k.command.Command
import ru.mdashlw.d4j4k.command.exceptionhandler.ExceptionHandler

object CommandWarningHandler : ExceptionHandler<Command.Warning>(Command.Warning::class) {
    override fun handle(command: Command, event: Command.Event, exception: Command.Warning) {
        event.replyWarning(exception.message ?: "(no message)").subscribe()
    }
}
