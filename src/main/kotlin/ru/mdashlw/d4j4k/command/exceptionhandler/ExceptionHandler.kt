package ru.mdashlw.d4j4k.command.exceptionhandler

import ru.mdashlw.d4j4k.command.Command
import ru.mdashlw.d4j4k.command.DiscordCommandClient
import kotlin.reflect.KClass

abstract class ExceptionHandler<in T : Throwable>(val type: KClass<in T>) {
    abstract fun handle(command: Command, event: Command.Event, exception: T)

    fun register() {
        DiscordCommandClient.INSTANCE.exceptionHandlers += this
    }
}
