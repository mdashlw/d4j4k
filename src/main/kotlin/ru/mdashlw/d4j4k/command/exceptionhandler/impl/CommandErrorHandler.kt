package ru.mdashlw.d4j4k.command.exceptionhandler.impl

import ru.mdashlw.d4j4k.command.Command
import ru.mdashlw.d4j4k.command.exceptionhandler.ExceptionHandler

object CommandErrorHandler : ExceptionHandler<Command.Error>(Command.Error::class) {
    override fun handle(command: Command, event: Command.Event, exception: Command.Error) {
        event.replyError(exception.message ?: "(no message)").subscribe()
    }
}
