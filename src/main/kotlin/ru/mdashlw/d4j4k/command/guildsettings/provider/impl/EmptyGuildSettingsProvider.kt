package ru.mdashlw.d4j4k.command.guildsettings.provider.impl

import discord4j.core.`object`.entity.Guild
import reactor.core.publisher.Mono
import ru.mdashlw.d4j4k.command.guildsettings.GuildSettings
import ru.mdashlw.d4j4k.command.guildsettings.impl.EmptyGuildSettings
import ru.mdashlw.d4j4k.command.guildsettings.provider.GuildSettingsProvider
import ru.mdashlw.d4j4k.extensions.toMono

object EmptyGuildSettingsProvider : GuildSettingsProvider {
    override fun provide(guild: Mono<Guild>): Mono<GuildSettings> = EmptyGuildSettings.toMono()
}
