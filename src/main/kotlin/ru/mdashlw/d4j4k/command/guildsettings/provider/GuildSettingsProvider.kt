package ru.mdashlw.d4j4k.command.guildsettings.provider

import discord4j.core.`object`.entity.Guild
import reactor.core.publisher.Mono
import ru.mdashlw.d4j4k.command.guildsettings.GuildSettings

interface GuildSettingsProvider {
    fun provide(guild: Mono<Guild>): Mono<GuildSettings>
}
