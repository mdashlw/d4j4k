package ru.mdashlw.d4j4k.command.guildsettings.impl

import ru.mdashlw.d4j4k.command.DiscordCommandClient
import ru.mdashlw.d4j4k.command.guildsettings.GuildSettings

object EmptyGuildSettings : GuildSettings {
    override val prefix: String = DiscordCommandClient.INSTANCE.prefix
}
