package ru.mdashlw.d4j4k.command

import discord4j.core.`object`.util.Snowflake
import discord4j.core.event.EventDispatcher
import discord4j.core.event.domain.message.MessageCreateEvent
import ru.mdashlw.d4j4k.command.colors.Colors
import ru.mdashlw.d4j4k.command.context.CommandContext
import ru.mdashlw.d4j4k.command.context.impl.BooleanContext
import ru.mdashlw.d4j4k.command.context.impl.IntContext
import ru.mdashlw.d4j4k.command.context.impl.LongContext
import ru.mdashlw.d4j4k.command.context.impl.TextChannelContext
import ru.mdashlw.d4j4k.command.emotes.Emotes
import ru.mdashlw.d4j4k.command.exceptionhandler.ExceptionHandler
import ru.mdashlw.d4j4k.command.exceptionhandler.impl.*
import ru.mdashlw.d4j4k.command.exceptionhandler.uncaught.UncaughtExceptionHandler
import ru.mdashlw.d4j4k.command.guildsettings.provider.GuildSettingsProvider
import ru.mdashlw.d4j4k.command.internal.findCommand
import ru.mdashlw.d4j4k.command.internal.handler.CommandHandler
import ru.mdashlw.d4j4k.command.replies.Replies
import ru.mdashlw.d4j4k.command.replymodifier.ReplyModifier
import ru.mdashlw.d4j4k.command.replymodifier.impl.ColorModifier
import ru.mdashlw.d4j4k.extensions.on
import ru.mdashlw.util.string.removeExtraSpaces

class DiscordCommandClient(
    val owner: Snowflake,
    val prefix: String,
    val replies: Replies,
    val colors: Colors,
    val emotes: Emotes,
    val guildSettingsProvider: GuildSettingsProvider,
    val uncaughtExceptionHandler: UncaughtExceptionHandler
) {
    val commands = mutableListOf<Command>()
    val contexts = mutableListOf<CommandContext<*>>()
    val exceptionHandlers = mutableListOf<ExceptionHandler<*>>()
    val replyModifiers = mutableListOf<ReplyModifier>()

    init {
        INSTANCE = this

        registerDefaultContexts()
        registerDefaultExceptionHandlers()
        registerDefaultReplyModifiers()
    }

    private fun registerDefaultContexts() {
        IntContext.register()
        BooleanContext.register()
        LongContext.register()
        TextChannelContext.register()
    }

    private fun registerDefaultExceptionHandlers() {
        CommandErrorHandler.register()
        CommandHelpHandler.register()
        CommandSuccessHandler.register()
        CommandWarningHandler.register()
        IllegalUsageHandler.register()
        NoAccessHandler.register()
        NoMemberPermissionsHandler.register()
        NoSelfPermissionsHandler.register()
    }

    private fun registerDefaultReplyModifiers() {
        ColorModifier.register()
    }

    @Suppress("NestedLambdaShadowedImplicitParameter", "NAME_SHADOWING", "LABEL_NAME_CLASH")
    fun subscribe(eventDispatcher: EventDispatcher) {
        eventDispatcher
            .on<MessageCreateEvent>()
            .filter { it.member.map { !it.isBot }.orElse(false) }
            .filter { it.message.content.isPresent }
            .subscribe {
                val message = it.message
                val guild = it.guild
                val content = message.content.get()

                guildSettingsProvider.provide(guild)
                    .filter { content.startsWith(it.prefix, true) }
                    .subscribe { guildSettings ->
                        val content = content.substring(guildSettings.prefix.length).trim().removeExtraSpaces()

                        val channel = message.channel
                        val member = it.member.get()
                        val args = content.split(" ")

                        val command = findCommand(args[0]) ?: return@subscribe
                        val event = command.Event(it.client, guild, guildSettings, channel, member, message)

                        CommandHandler.handle(command, event, args.drop(1)).subscribe()
                    }
            }
    }

    companion object {
        // TODO Change it somehow
        lateinit var INSTANCE: DiscordCommandClient
            private set
    }
}
