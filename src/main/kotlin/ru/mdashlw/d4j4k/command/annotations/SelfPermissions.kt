package ru.mdashlw.d4j4k.command.annotations

import discord4j.core.`object`.util.Permission

@Target(AnnotationTarget.CLASS)
annotation class SelfPermissions(vararg val value: Permission)
