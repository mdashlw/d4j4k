package ru.mdashlw.d4j4k.command.annotations

@Target(AnnotationTarget.FUNCTION)
annotation class CommandFunction
