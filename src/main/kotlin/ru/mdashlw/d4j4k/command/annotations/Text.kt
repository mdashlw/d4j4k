package ru.mdashlw.d4j4k.command.annotations

@Target(AnnotationTarget.VALUE_PARAMETER)
@Retention
annotation class Text
