package ru.mdashlw.d4j4k.command.annotations

import discord4j.core.`object`.util.Permission

@Target(AnnotationTarget.CLASS)
annotation class MemberPermissions(vararg val value: Permission)
