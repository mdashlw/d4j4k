package ru.mdashlw.d4j4k.command.annotations

@Target(AnnotationTarget.CLASS)
annotation class Aliases(vararg val value: String)
