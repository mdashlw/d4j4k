package ru.mdashlw.d4j4k.command.annotations

@Target(AnnotationTarget.CLASS)
annotation class Description(val value: String)
