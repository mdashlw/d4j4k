package ru.mdashlw.d4j4k.command.commands

import reactor.core.publisher.Mono
import ru.mdashlw.d4j4k.command.Command
import ru.mdashlw.d4j4k.command.DiscordCommandClient
import ru.mdashlw.d4j4k.command.annotations.Aliases
import ru.mdashlw.d4j4k.command.annotations.CommandFunction
import ru.mdashlw.d4j4k.command.annotations.Description
import ru.mdashlw.d4j4k.command.annotations.Name

@Name("help")
@Aliases("commands", "cmds")
@Description("Displays all the commands")
object HelpCommand : Command() {
    @CommandFunction
    fun Event.help(): Mono<Void> {
        val commands = DiscordCommandClient.INSTANCE.commands
            .filter { it.meta.displayInHelp }

        return client.self
            .flatMap {
                reply { embed ->
                    embed.setTitle("${it.username} commands")
                    // embed.setDescription("**Use `${guildSettings.prefix}help <command>` for additional info.**")
                    embed.addField(
                        "Command",
                        commands.joinToString("\n", "**", "**") { it.meta.name },
                        true
                    )
                    embed.addField(
                        "Description",
                        commands.joinToString("\n") { it.meta.description },
                        true
                    )
                }
            }
            .then()
    }
}
