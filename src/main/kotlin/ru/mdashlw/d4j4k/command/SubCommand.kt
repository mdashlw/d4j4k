package ru.mdashlw.d4j4k.command

abstract class SubCommand(val parent: Command) : Command() {
    override fun register() {
        parent.subCommands += this
    }
}
