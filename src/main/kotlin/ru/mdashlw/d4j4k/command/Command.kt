package ru.mdashlw.d4j4k.command

import discord4j.core.DiscordClient
import discord4j.core.`object`.entity.Guild
import discord4j.core.`object`.entity.Member
import discord4j.core.`object`.entity.Message
import discord4j.core.`object`.entity.MessageChannel
import discord4j.core.spec.EmbedCreateSpec
import reactor.core.publisher.Mono
import ru.mdashlw.d4j4k.command.annotations.CommandFunction
import ru.mdashlw.d4j4k.command.guildsettings.GuildSettings
import ru.mdashlw.d4j4k.command.internal.meta.CommandMetaResolver
import ru.mdashlw.d4j4k.extensions.toFlux
import ru.mdashlw.d4j4k.extensions.toMono
import ru.mdashlw.d4j4k.util.combine
import ru.mdashlw.d4j4k.util.mapAs
import kotlin.reflect.KFunction
import kotlin.reflect.full.findAnnotation
import kotlin.reflect.full.functions

@Suppress("LeakingThis")
abstract class Command {
    val functions: Sequence<KFunction<Mono<Void>>> =
        this::class.functions
            .asSequence()
            .filter { it.findAnnotation<CommandFunction>() != null }
            .mapAs<KFunction<Mono<Void>>>()
            .sortedByDescending { it.parameters.size }

    val subCommands = mutableListOf<SubCommand>()

    val meta by lazy { CommandMetaResolver.resolve(this) }

    open fun register() {
        DiscordCommandClient.INSTANCE.commands += this
    }

    open fun checkAccess(event: Event): Mono<Boolean> = true.toMono()

    inner class Event(
        val client: DiscordClient,
        val guild: Mono<Guild>,
        val guildSettings: GuildSettings,
        val channel: Mono<MessageChannel>,
        val member: Member,
        val message: Message
    ) {
        fun reply(message: String): Mono<Message> =
            channel.flatMap { it.createMessage(message) }

        fun reply(spec: (embed: EmbedCreateSpec) -> Unit): Mono<Message> =
            DiscordCommandClient.INSTANCE.replyModifiers.toFlux()
                .filterWhen { it.check(this@Command, this@Event) }
                .flatMap { it.modify(this@Command, this@Event) }
                .reduce { t, u -> t.combine(u) }
                .map { it.combine(spec) }
                .flatMap { newSpec -> channel.flatMap { it.createEmbed(newSpec) } }

        fun replyHelp(): Mono<Message> =
            DiscordCommandClient.INSTANCE.replies.help(this@Command, this@Event)

        fun replySuccess(message: String): Mono<Message> =
            DiscordCommandClient.INSTANCE.replies.success(this@Command, this@Event, message)

        fun replyWarning(message: String): Mono<Message> =
            DiscordCommandClient.INSTANCE.replies.warning(this@Command, this@Event, message)

        fun replyError(message: String): Mono<Message> =
            DiscordCommandClient.INSTANCE.replies.error(this@Command, this@Event, message)

        fun copy(command: Command): Event =
            command.Event(client, guild, guildSettings, channel, member, message)
    }

    class Help : Exception()

    class Success(message: String) : Exception(message)

    class Warning(message: String) : Exception(message)

    class Error(message: String) : Exception(message)
}
