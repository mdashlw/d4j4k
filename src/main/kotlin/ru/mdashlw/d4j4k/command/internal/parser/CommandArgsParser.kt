package ru.mdashlw.d4j4k.command.internal.parser

import reactor.core.publisher.Mono
import ru.mdashlw.d4j4k.command.Command
import ru.mdashlw.d4j4k.command.annotations.Text
import ru.mdashlw.d4j4k.command.context.CommandContext
import ru.mdashlw.d4j4k.command.internal.findCommandContext
import ru.mdashlw.d4j4k.extensions.toFlux
import ru.mdashlw.d4j4k.extensions.toMono
import kotlin.reflect.KFunction
import kotlin.reflect.KParameter
import kotlin.reflect.full.createType
import kotlin.reflect.full.findAnnotation
import kotlin.reflect.full.withNullability

internal object CommandArgsParser {
    @Suppress("NAME_SHADOWING", "NestedLambdaShadowedImplicitParameter")
    fun parse(
        command: Command,
        event: Command.Event,
        function: KFunction<Mono<Void>>,
        args: List<String>
    ): Mono<Map<KParameter, Any>> {
        var args = args
        val parameters = function.parameters
        val parameterCount = parameters.size

        return parameters.toFlux()
            .flatMap {
                val index = it.index
                val type = it.type.withNullability(false)

                when (it.kind) {
                    KParameter.Kind.INSTANCE -> (it to command).toMono()
                    KParameter.Kind.EXTENSION_RECEIVER -> (it to event).toMono()
                    KParameter.Kind.VALUE -> {
                        val result =
                            if (type == String::class.createType()) {
                                if (it.findAnnotation<Text>() != null) {
                                    val argsToDrop = args.size - parameterCount + index + 1

                                    CommandContext.Result(args.drop(argsToDrop).joinToString(" "), argsToDrop).toMono()
                                } else {
                                    CommandContext.Result(args.first()).toMono()
                                }
                            } else {
                                val context = findCommandContext<Any>(type)
                                    ?: throw NotImplementedError("No command context for $type")

                                if (args.isEmpty()) {
                                    if (it.isOptional) null
                                    else {
                                        context.resolve(event)
                                    }
                                } else {
                                    context.resolve(
                                        event,
                                        args.first(),
                                        args.subList(0, args.size - parameterCount + index + 1).joinToString(" ")
                                    )
                                }
                            }

                        result
                            ?.doOnSuccess { args = args.drop(it.args) }
                            ?.map { r -> it to r.`object` }
                            ?: Mono.empty()
                    }
                }
            }
            .collectList()
            .map(List<Pair<KParameter, Any>>::toMap)
    }
}
