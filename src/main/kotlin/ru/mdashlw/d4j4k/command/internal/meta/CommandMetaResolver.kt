package ru.mdashlw.d4j4k.command.internal.meta

import discord4j.core.`object`.util.PermissionSet
import ru.mdashlw.d4j4k.command.Command
import ru.mdashlw.d4j4k.command.SubCommand
import ru.mdashlw.d4j4k.command.annotations.*
import ru.mdashlw.d4j4k.command.meta.CommandMeta
import kotlin.reflect.KClass
import kotlin.reflect.full.findAnnotation

internal object CommandMetaResolver {
    fun resolve(command: Command, commandClass: KClass<out Command>): CommandMeta =
        CommandMeta(
            resolveName(commandClass),
            resolveAliases(commandClass),
            resolveDescription(commandClass),
            resolveUsage(command, commandClass),
            resolveExamples(command, commandClass),
            resolveMemberPermissions(command, commandClass),
            resolveSelfPermissions(command, commandClass),
            resolveDisplayInHelp(commandClass),
            resolveSendTyping(commandClass)
        )

    fun resolve(command: Command) = resolve(command, command::class)

    fun resolveName(commandClass: KClass<out Command>): String =
        commandClass.findAnnotation<Name>()?.value
            ?: throw IllegalArgumentException("Command does not have name")

    fun resolveAliases(commandClass: KClass<out Command>): List<String> =
        commandClass.findAnnotation<Aliases>()?.value?.toList().orEmpty()

    fun resolveDescription(commandClass: KClass<out Command>): String =
        commandClass.findAnnotation<Description>()?.value
            ?: throw IllegalArgumentException("Command does not have description")

    fun resolveUsage(command: Command, commandClass: KClass<out Command>): String {
        val name = resolveNames(command, commandClass).joinToString(" ", postfix = " ")

        return (name + commandClass.findAnnotation<Usage>()?.value.orEmpty()).trim()
    }

    fun resolveExamples(command: Command, commandClass: KClass<out Command>): List<String> {
        val name = resolveNames(command, commandClass).joinToString(" ", postfix = " ")

        return commandClass.findAnnotation<Examples>()?.value?.toList()?.map { (name + it).trim() }.orEmpty()
    }

    fun resolveMemberPermissions(command: Command, commandClass: KClass<out Command>): PermissionSet =
        commandClass.findAnnotation<MemberPermissions>()?.value?.let { PermissionSet.of(*it) }
            ?: if (command is SubCommand) {
                command.parent.meta.memberPermissions
            } else {
                PermissionSet.none()
            }

    fun resolveSelfPermissions(command: Command, commandClass: KClass<out Command>): PermissionSet =
        commandClass.findAnnotation<SelfPermissions>()?.value?.let { PermissionSet.of(*it) }
            ?: if (command is SubCommand) {
                command.parent.meta.selfPermissions
            } else {
                PermissionSet.none()
            }

    fun resolveDisplayInHelp(commandClass: KClass<out Command>): Boolean =
        commandClass.findAnnotation<DoNotDisplayInHelp>() == null

    fun resolveSendTyping(commandClass: KClass<out Command>): Boolean =
        commandClass.findAnnotation<SendTyping>() != null

    private fun resolveNames(command: Command, commandClass: KClass<out Command>): List<String> {
        val names = mutableListOf(resolveName(commandClass))

        if (command is SubCommand) {
            names += resolveNames(command.parent, command.parent::class)
        }

        return names.reversed()
    }
}
