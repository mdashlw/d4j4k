package ru.mdashlw.d4j4k.command.internal.executor

import reactor.core.publisher.Mono
import ru.mdashlw.d4j4k.command.Command
import ru.mdashlw.d4j4k.command.context.CommandContext
import ru.mdashlw.d4j4k.command.exceptions.CommandException
import ru.mdashlw.d4j4k.command.exceptions.IllegalUsageException
import ru.mdashlw.d4j4k.command.internal.findFunction
import ru.mdashlw.d4j4k.command.internal.parser.CommandArgsParser
import ru.mdashlw.d4j4k.extensions.onErrorMap
import ru.mdashlw.d4j4k.extensions.toMono
import java.lang.reflect.InvocationTargetException
import java.util.concurrent.ExecutionException
import kotlin.reflect.KFunction

internal object CommandExecutor {
    private fun execute(
        command: Command,
        event: Command.Event,
        function: KFunction<Mono<Void>>,
        args: List<String> = emptyList()
    ): Mono<Void> {
        if (command.meta.sendTyping) {
            event.channel
                .flatMap { it.type() }
                .subscribe()
        }

        return CommandArgsParser.parse(command, event, function, args)
            .flatMap(function::callBy)
            .onErrorMap(CommandContext.Error::class) { IllegalUsageException(it.message ?: "(no message)") }
            .onErrorMap(InvocationTargetException::class) { it.cause ?: it }
            .onErrorMap(ExecutionException::class) { it.cause ?: it }
            .onErrorMap { CommandException(command, event, it) }
    }

    fun execute(command: Command, event: Command.Event, args: List<String> = emptyList()): Mono<Void> {
        val function = command.findFunction(args.size)
            ?: command.findFunction(args.size + 1)
            ?: return CommandException(command, event, Command.Help()).toMono()

        return execute(command, event, function, args)
    }
}
