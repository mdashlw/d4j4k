package ru.mdashlw.d4j4k.command.internal

import reactor.core.publisher.Mono
import ru.mdashlw.d4j4k.command.Command
import ru.mdashlw.d4j4k.command.DiscordCommandClient
import ru.mdashlw.d4j4k.command.context.CommandContext
import ru.mdashlw.d4j4k.command.exceptionhandler.ExceptionHandler
import ru.mdashlw.util.list.containsIgnoreCase
import kotlin.reflect.KClass
import kotlin.reflect.KFunction
import kotlin.reflect.KParameter
import kotlin.reflect.KType
import kotlin.reflect.full.valueParameters

internal fun findCommand(name: String, parent: Command? = null): Command? =
    (parent?.subCommands ?: DiscordCommandClient.INSTANCE.commands)
        .find {
            it.meta.name.equals(name, true) || it.meta.aliases.containsIgnoreCase(name)
        }

internal fun Command.findFunction(args: Int): KFunction<Mono<Void>>? =
    functions.find { it.valueParameters.size <= args }
        ?: functions.find { it.valueParameters.filterNot(KParameter::isOptional).size <= args }

@Suppress("UNCHECKED_CAST")
@Throws(NotImplementedError::class)
internal fun <T : Any> findCommandContext(type: KType): CommandContext<T>? =
    DiscordCommandClient.INSTANCE.contexts
        .find { it.type == type } as? CommandContext<T>

@Suppress("UNCHECKED_CAST")
internal fun <T : Throwable> findExceptionHandler(type: KClass<T>): ExceptionHandler<T>? =
    DiscordCommandClient.INSTANCE.exceptionHandlers
        .find { it.type == type } as? ExceptionHandler<T>
