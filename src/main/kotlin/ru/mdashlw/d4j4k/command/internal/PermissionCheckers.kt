@file:Suppress("NOTHING_TO_INLINE")

package ru.mdashlw.d4j4k.command.internal

import discord4j.core.`object`.entity.Member
import reactor.core.publisher.Mono
import ru.mdashlw.d4j4k.command.Command

internal inline fun Command.checkMemberPermissions(member: Member): Mono<Boolean> =
    member.basePermissions
        .map { it.containsAll(meta.memberPermissions) }

internal inline fun Command.checkSelfPermissions(selfMember: Member): Mono<Boolean> =
    selfMember.basePermissions
        .map { it.containsAll(meta.selfPermissions) }
