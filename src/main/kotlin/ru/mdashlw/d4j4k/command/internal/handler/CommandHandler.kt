package ru.mdashlw.d4j4k.command.internal.handler

import discord4j.core.`object`.util.Snowflake
import reactor.core.publisher.Mono
import ru.mdashlw.d4j4k.command.Command
import ru.mdashlw.d4j4k.command.DiscordCommandClient
import ru.mdashlw.d4j4k.command.exceptions.CommandException
import ru.mdashlw.d4j4k.command.exceptions.NoAccessException
import ru.mdashlw.d4j4k.command.exceptions.NoMemberPermissionsException
import ru.mdashlw.d4j4k.command.exceptions.NoSelfPermissionsException
import ru.mdashlw.d4j4k.command.internal.checkMemberPermissions
import ru.mdashlw.d4j4k.command.internal.checkSelfPermissions
import ru.mdashlw.d4j4k.command.internal.executor.CommandExecutor
import ru.mdashlw.d4j4k.command.internal.findCommand
import ru.mdashlw.d4j4k.command.internal.findExceptionHandler
import ru.mdashlw.d4j4k.extensions.doOnError
import ru.mdashlw.d4j4k.extensions.toMono
import java.util.*

internal object CommandHandler {
    private fun check(command: Command, event: Command.Event): Mono<Unit> =
        Unit.toMono()
            .filterWhen { command.checkAccess(event) }
            .switchIfEmpty(NoAccessException().toMono())
            .filterWhen { command.checkMemberPermissions(event.member) }
            .switchIfEmpty(NoMemberPermissionsException().toMono())
            .map { event.client.selfId }
            .filter(Optional<Snowflake>::isPresent)
            .map(Optional<Snowflake>::get)
            .flatMap { id -> event.guild.flatMap { it.getMemberById(id) } }
            .filterWhen(command::checkSelfPermissions)
            .switchIfEmpty(NoSelfPermissionsException().toMono())
            .onErrorMap { CommandException(command, event, it) }
            .map { Unit }

    private fun execute(command: Command, event: Command.Event, args: List<String>): Mono<Void> {
        if (args.isEmpty()) {
            return CommandExecutor.execute(command, event)
        }

        val subCommand = findCommand(args[0], command)

        if (subCommand != null) {
            val subEvent = event.copy(subCommand)

            return check(subCommand, subEvent)
                .flatMap { execute(subCommand, subEvent, args.drop(1)) }
        }

        return CommandExecutor.execute(command, event, args)
    }

    fun handle(command: Command, event: Command.Event, args: List<String>): Mono<Void> =
        check(command, event)
            .flatMap { execute(command, event, args) }
            .doOnError(CommandException::class) {
                val exception = it.cause!!

                val handler = findExceptionHandler(exception.javaClass.kotlin)
                    ?: DiscordCommandClient.INSTANCE.uncaughtExceptionHandler

                handler.handle(it.command, it.event, exception)
            }
            .then()
}
