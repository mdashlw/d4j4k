package ru.mdashlw.d4j4k.command.meta

import discord4j.core.`object`.util.PermissionSet

data class CommandMeta(
    val name: String,
    val aliases: List<String>,
    val description: String,
    val usage: String,
    val examples: List<String>,
    val memberPermissions: PermissionSet,
    val selfPermissions: PermissionSet,
    val displayInHelp: Boolean,
    val sendTyping: Boolean
)
