package ru.mdashlw.d4j4k.command.context

import reactor.core.publisher.Mono
import ru.mdashlw.d4j4k.command.Command
import ru.mdashlw.d4j4k.command.DiscordCommandClient
import kotlin.reflect.KType

abstract class CommandContext<T : Any>(val type: KType) {
    @Throws(Error::class)
    abstract fun resolve(event: Command.Event, arg: String, text: String): Mono<out Result<T>>

    @Throws(Command.Help::class)
    open fun resolve(event: Command.Event): Mono<out Result<T>> = throw Command.Help()

    fun register() {
        DiscordCommandClient.INSTANCE.contexts += this
    }

    class Result<out T : Any>(val `object`: T, val args: Int = 1)

    class Error(message: String) : Exception(message)
}
