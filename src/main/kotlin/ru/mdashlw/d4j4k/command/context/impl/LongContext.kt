package ru.mdashlw.d4j4k.command.context.impl

import reactor.core.publisher.Mono
import ru.mdashlw.d4j4k.command.Command
import ru.mdashlw.d4j4k.command.context.CommandContext
import ru.mdashlw.d4j4k.extensions.toMono
import ru.mdashlw.util.string.removeNumberFormat
import kotlin.reflect.full.createType

object LongContext : CommandContext<Long>(Long::class.createType()) {
    override fun resolve(event: Command.Event, arg: String, text: String): Mono<out Result<Long>> =
        arg.removeNumberFormat().toLongOrNull()?.let { Result(it) }?.toMono()
            ?: throw Error("Input is not a number")
}
