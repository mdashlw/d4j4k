package ru.mdashlw.d4j4k.command.context.impl

import discord4j.core.`object`.entity.TextChannel
import reactor.core.publisher.Mono
import ru.mdashlw.d4j4k.command.Command
import ru.mdashlw.d4j4k.command.context.CommandContext
import ru.mdashlw.d4j4k.extensions.*
import kotlin.reflect.full.createType

object TextChannelContext : CommandContext<TextChannel>(TextChannel::class.createType()) {
    @Suppress("NestedLambdaShadowedImplicitParameter")
    override fun resolve(event: Command.Event, arg: String, text: String): Mono<out Result<TextChannel>> =
        event.guild
            .flatMapMany { it.channels }
            .filter { it.name.equals(arg, true) }
            .next()
            .cast<TextChannel>()
            .switchIfEmpty {
                arg.toLongOrNull()
                    ?.let {
                        val snowflake = it.asSnowflake()

                        event.guild
                            .flatMap { it.getChannelById(snowflake) }
                            .cast<TextChannel>()
                    }
                    ?: Mono.empty()
            }
            .switchIfEmpty {
                event.message.channelMentions.next()
            }
            .onErrorResume(ClassCastException::class) { Mono.empty() }
            .map { Result(it) }
            .switchIfEmpty(Error("Channel #$text does not exist").toMono())
}
