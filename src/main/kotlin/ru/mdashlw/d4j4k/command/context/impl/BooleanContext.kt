package ru.mdashlw.d4j4k.command.context.impl

import reactor.core.publisher.Mono
import ru.mdashlw.d4j4k.command.Command
import ru.mdashlw.d4j4k.command.context.CommandContext
import ru.mdashlw.d4j4k.extensions.toMono
import ru.mdashlw.util.string.isBoolean
import kotlin.reflect.full.createType

object BooleanContext : CommandContext<Boolean>(Boolean::class.createType()) {
    override fun resolve(event: Command.Event, arg: String, text: String): Mono<out Result<Boolean>> =
        arg.takeIf(String::isBoolean)?.toBoolean()?.let { Result(it) }?.toMono()
            ?: throw Error("Input is not a boolean")
}
