package ru.mdashlw.d4j4k.command.emotes.impl

import ru.mdashlw.d4j4k.command.emotes.Emotes

object DefaultEmotes : Emotes {
    override var success: String = ""
    override var warning: String = ""
    override var error: String = ""
}
