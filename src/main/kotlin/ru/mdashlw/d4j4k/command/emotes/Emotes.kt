package ru.mdashlw.d4j4k.command.emotes

interface Emotes {
    var success: String
    var warning: String
    var error: String
}
