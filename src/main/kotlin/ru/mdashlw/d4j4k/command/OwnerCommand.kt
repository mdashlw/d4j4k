package ru.mdashlw.d4j4k.command

import discord4j.core.`object`.util.Snowflake

abstract class OwnerCommand : PrivateCommand() {
    override val users: Set<Snowflake> = emptySet()
}
