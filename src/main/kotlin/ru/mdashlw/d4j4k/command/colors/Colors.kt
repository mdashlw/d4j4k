package ru.mdashlw.d4j4k.command.colors

import java.awt.Color

interface Colors {
    var default: Color?
    var success: Color
    var warning: Color
    var error: Color
}
