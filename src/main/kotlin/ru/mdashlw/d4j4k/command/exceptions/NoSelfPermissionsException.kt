package ru.mdashlw.d4j4k.command.exceptions

class NoSelfPermissionsException : RuntimeException()
