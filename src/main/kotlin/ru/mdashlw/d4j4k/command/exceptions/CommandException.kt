package ru.mdashlw.d4j4k.command.exceptions

import ru.mdashlw.d4j4k.command.Command

class CommandException(val command: Command, val event: Command.Event, cause: Throwable) : RuntimeException(cause)
