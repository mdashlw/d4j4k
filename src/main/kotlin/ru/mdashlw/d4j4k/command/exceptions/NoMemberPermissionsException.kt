package ru.mdashlw.d4j4k.command.exceptions

class NoMemberPermissionsException : RuntimeException()
