package ru.mdashlw.d4j4k.command.exceptions

class IllegalUsageException(message: String) : RuntimeException(message)
