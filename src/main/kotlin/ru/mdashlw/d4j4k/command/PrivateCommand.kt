package ru.mdashlw.d4j4k.command

import discord4j.core.`object`.util.Snowflake
import reactor.core.publisher.Mono
import ru.mdashlw.d4j4k.extensions.toMono

abstract class PrivateCommand : Command() {
    abstract val users: Set<Snowflake>

    override fun checkAccess(event: Event): Mono<Boolean> =
        (event.member.id.run { equals(DiscordCommandClient.INSTANCE.owner) || this in users }).toMono()
}
