package ru.mdashlw.d4j4k.command.replies

import discord4j.core.`object`.entity.Message
import reactor.core.publisher.Mono
import ru.mdashlw.d4j4k.command.Command

interface Replies {
    var help: (command: Command, event: Command.Event) -> Mono<Message>

    var success: (command: Command, event: Command.Event, message: String) -> Mono<Message>

    var warning: (command: Command, event: Command.Event, message: String) -> Mono<Message>

    var error: (command: Command, event: Command.Event, message: String) -> Mono<Message>
}
