package ru.mdashlw.d4j4k.command.replies.impl

import discord4j.core.`object`.entity.Message
import discord4j.core.`object`.util.Permission
import reactor.core.publisher.Mono
import ru.mdashlw.d4j4k.command.Command
import ru.mdashlw.d4j4k.command.DiscordCommandClient
import ru.mdashlw.d4j4k.command.replies.Replies
import ru.mdashlw.d4j4k.extensions.addBlankField

object DefaultReplies : Replies {
    @Suppress("NestedLambdaShadowedImplicitParameter")
    override var help: (command: Command, event: Command.Event) -> Mono<Message> =
        { command, event ->
            event.client.self
                .flatMap { self ->
                    event.reply { embed ->
                        embed.setThumbnail(self.avatarUrl)
                        embed.setTitle("`${command.meta.name}`")
                        embed.setDescription(command.meta.description)
                        embed.setFooter(
                            "Do not include <> or [] — They indicate <required> and [optional] arguments.",
                            null
                        )

                        command.meta.aliases
                            .takeIf(List<String>::isNotEmpty)
                            ?.let {
                                embed.addField("Aliases", it.joinToString(), false)
                            }

                        embed.addBlankField(false)
                        embed.addField(
                            "Usage",
                            "`${event.guildSettings.prefix}${command.meta.usage}`",
                            false
                        )

                        command.meta.examples
                            .takeIf(List<String>::isNotEmpty)
                            ?.let {
                                embed.addField(
                                    "Examples",
                                    it.joinToString("\n") { "${event.guildSettings.prefix}$it" },
                                    false
                                )
                            }

                        if (command.meta.memberPermissions.isNotEmpty() || command.meta.selfPermissions.isNotEmpty()) {
                            embed.addBlankField(false)

                            command.meta.memberPermissions
                                .takeIf { it.rawValue != 0L }
                                ?.let {
                                    embed.addField(
                                        "Required Permissions",
                                        it.joinToString("\n", "**", "**", transform = Permission::name),
                                        true
                                    )
                                }

                            command.meta.selfPermissions
                                .takeIf { it.rawValue != 0L }
                                ?.let {
                                    embed.addField(
                                        "Bot Permissions",
                                        it.joinToString("\n", "**", "**", transform = Permission::name),
                                        true
                                    )
                                }
                        }
                    }
                }
        }

    override var success: (command: Command, event: Command.Event, message: String) -> Mono<Message> =
        { _, event, message ->
            event.reply { embed ->
                embed.setColor(DiscordCommandClient.INSTANCE.colors.success)
                embed.setTitle("Success")
                embed.setDescription("${DiscordCommandClient.INSTANCE.emotes.success} $message")
            }
        }

    override var warning: (command: Command, event: Command.Event, message: String) -> Mono<Message> =
        { _, event, message ->
            event.reply { embed ->
                embed.setColor(DiscordCommandClient.INSTANCE.colors.warning)
                embed.setTitle("Warning")
                embed.setDescription("${DiscordCommandClient.INSTANCE.emotes.warning} $message")
            }
        }

    override var error: (command: Command, event: Command.Event, message: String) -> Mono<Message> =
        { _, event, message ->
            event.reply { embed ->
                embed.setColor(DiscordCommandClient.INSTANCE.colors.error)
                embed.setTitle("Error")
                embed.setDescription("${DiscordCommandClient.INSTANCE.emotes.error} $message")
            }
        }
}
