package ru.mdashlw.d4j4k.dsl.marker

@DslMarker
@Target(AnnotationTarget.CLASS)
internal annotation class DiscordDslMarker
