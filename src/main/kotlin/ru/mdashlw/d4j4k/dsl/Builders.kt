@file:Suppress("FunctionName")

package ru.mdashlw.d4j4k.dsl

import discord4j.core.DiscordClient
import discord4j.core.DiscordClientBuilder
import ru.mdashlw.d4j4k.command.DiscordCommandClient
import ru.mdashlw.d4j4k.dsl.impl.DiscordCommandClientBuilder

inline fun DiscordClient(token: String, scope: DiscordClientBuilder.() -> Unit = {}): DiscordClient =
    DiscordClientBuilder(token).apply(scope).build()

inline fun DiscordCommandClient(
    client: DiscordClient,
    scope: DiscordCommandClientBuilder.() -> Unit = {}
): DiscordCommandClient =
    DiscordCommandClientBuilder(client).apply(scope).build()
