package ru.mdashlw.d4j4k.dsl.impl

import discord4j.core.DiscordClient
import discord4j.core.`object`.util.Snowflake
import ru.mdashlw.d4j4k.command.DiscordCommandClient
import ru.mdashlw.d4j4k.command.colors.Colors
import ru.mdashlw.d4j4k.command.colors.impl.DefaultColors
import ru.mdashlw.d4j4k.command.emotes.Emotes
import ru.mdashlw.d4j4k.command.emotes.impl.DefaultEmotes
import ru.mdashlw.d4j4k.command.exceptionhandler.uncaught.UncaughtExceptionHandler
import ru.mdashlw.d4j4k.command.exceptionhandler.uncaught.impl.DefaultUncaughtExceptionHandler
import ru.mdashlw.d4j4k.command.guildsettings.provider.GuildSettingsProvider
import ru.mdashlw.d4j4k.command.guildsettings.provider.impl.EmptyGuildSettingsProvider
import ru.mdashlw.d4j4k.command.replies.Replies
import ru.mdashlw.d4j4k.command.replies.impl.DefaultReplies
import ru.mdashlw.d4j4k.dsl.Dsl
import ru.mdashlw.d4j4k.dsl.marker.DiscordDslMarker

@DiscordDslMarker
class DiscordCommandClientBuilder(private val client: DiscordClient) : Dsl<DiscordCommandClient> {
    lateinit var owner: Snowflake
    lateinit var prefix: String
    var replies: Replies = DefaultReplies
    var colors: Colors = DefaultColors
    var emotes: Emotes = DefaultEmotes
    var guildSettingsProvider: GuildSettingsProvider = EmptyGuildSettingsProvider
    var uncaughtExceptionHandler: UncaughtExceptionHandler = DefaultUncaughtExceptionHandler

    inline fun replies(scope: Replies.() -> Unit) {
        replies = replies.apply(scope)
    }

    inline fun colors(scope: Colors.() -> Unit) {
        colors = colors.apply(scope)
    }

    inline fun emotes(scope: Emotes.() -> Unit) {
        emotes = emotes.apply(scope)
    }

    override fun build(): DiscordCommandClient =
        DiscordCommandClient(
            owner,
            prefix,
            replies,
            colors,
            emotes,
            guildSettingsProvider,
            uncaughtExceptionHandler
        ).also {
            it.subscribe(client.eventDispatcher)
        }
}
