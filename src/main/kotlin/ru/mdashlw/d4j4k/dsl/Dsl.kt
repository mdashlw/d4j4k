package ru.mdashlw.d4j4k.dsl

interface Dsl<T : Any> {
    fun build(): T
}
